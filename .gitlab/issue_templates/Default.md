
## From Idea to Production

* [ ] Update a file


### Potential Scenario

1. AE receives a new lead via an IQM as an "issue"
1. AE has a quick call with the customer (watches a video) and needs to submit an SA Request (as an MR)
1. The AE creates an MR from the Issue and a pipeline initiates
1. SFDC Check "Job" passes but the pipeline fails because certain information needs to be updated in the Command Plan. 
1. The AE updates a YAML file for the command plan. 
1. Command Plan "Job" now passes


### Other ideas...

1. the threshold for approval is too high, so they need to seek approval from their manager.
